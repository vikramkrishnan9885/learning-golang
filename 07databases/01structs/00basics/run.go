package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "root:1q2w3e4r!@tcp(127.0.0.1:3306)/test")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Connection established")
	}

	// IN THE PREVIOUS EXAMPLE WE HAD var database string
	type Timeline struct {
		Id      int
		Content string
	}
	rows, err := db.Query(`SELECT id, content FROM timeline`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		// CREATE AN EMPTY STRUCT
		timeline := Timeline{}
		// SEE THE INPUTS TO THE SCAN
		err = rows.Scan(&timeline.Id, &timeline.Content)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(timeline)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
}
