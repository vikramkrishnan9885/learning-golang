package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/guregu/null.v3"
)

type ExpenseReport struct {
	Id     int         `json:"id"` // Paramter to be injected into a query cannot be null
	TripId null.Int    `json:"tripId"`
	Title  null.String `json."title"`
}

func GetExpenseReportById(db *sql.DB, id int) (*ExpenseReport, error) {
	const query = `SELECT trip_id,title from expense_reports where id = ? `
	var retval ExpenseReport
	err := db.QueryRow(query, id).Scan(&retval.TripId, &retval.Title)
	retval.Id = id
	return &retval, err
}

func main() {
	db, err := sql.Open("mysql", "root:1q2w3e4r!@tcp(127.0.0.1:3306)/test")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	for i := 1; i != 3; i++ {
		expenseReport, err := GetExpenseReportById(db, i)
		expenseReportJson, _ := json.Marshal(expenseReport)
		if err != nil {
			fmt.Fprintf(os.Stdout, "Error:%s\n", err)
		} else {
			fmt.Fprintf(os.Stdout, "Expense Report:%v\n", string(expenseReportJson))
			fmt.Println("When you print out the struct note the nesting that the null package brings")
			fmt.Fprintf(os.Stdout, "Expense Report:%v\n", *expenseReport)
		}
	}
}
