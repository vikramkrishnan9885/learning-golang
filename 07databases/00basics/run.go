package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func main() {

	// STEP 0: HANDLE DATABASE CONNECTIONS
	db, err := sql.Open("mysql", "root:1q2w3e4r!@tcp(127.0.0.1:3306)/test")
	if err != nil {
		panic(err)
	} else {
		fmt.Println("Connection established")
	}
	db.SetConnMaxLifetime(500)
	db.SetMaxOpenConns(15)
	db.SetMaxIdleConns(50)
	/*
		->	Connection pooling means that executing two consecutive statements on a
			single database might open two connections and execute them separately.
			It is fairly common for programmers to be confused as to why their code
			misbehaves. For example, LOCK TABLES followed by an INSERT can block
			because the INSERT is on a connection that does not hold the table lock.
		->	Connections are created when needed and there isn't a free connection
			in the pool.
		->	By default, there's no limit on the number of connections. If you
			try to do a lot of things at once, you can create an arbitrary number
			of connections. This can cause the database to return an error such as
			"too many connections."
		->	In Go 1.1 or newer, you can use db.SetMaxIdleConns(N) to limit
			the number of idle connections in the pool. This doesn't limit
			the pool size, though.
		->	In Go 1.2.1 or newer, you can use db.SetMaxOpenConns(N) to limit
			the number of total open connections to the database. Unfortunately,
			a deadlock bug (fix) prevents db.SetMaxOpenConns(N) from safely being
			used in 1.2.
		->	Connections are recycled rather fast. Setting a high number of
			idle connections with db.SetMaxIdleConns(N) can reduce this churn,
			and help keep connections around for reuse.
		->	Keeping a connection idle for a long time can cause problems (like
			in this issue with MySQL on Microsoft Azure). Try db.SetMaxIdleConns(0)
			if you get connection timeouts because a connection is idle for too long.
		->	You can also specify the maximum amount of time a connection may be
			reused by setting db.SetConnMaxLifetime(duration) since reusing long
			lived connections may cause network issues. This closes the unused
			connections lazily i.e. closing expired connection may be deferred.
	*/

	// Note the defer
	defer db.Close()
	/*
		In the example shown, we're illustrating several things:
			->	The first argument to sql.Open is the driver name. This is the
				string that the driver used to register itself with database/sql,
				and is conventionally the same as the package name to avoid confusion.
				For example, it's mysql for github.com/go-sql-driver/mysql.
				Some drivers do not follow the convention and use the database name,
				e.g. sqlite3 for github.com/mattn/go-sqlite3 and postgres for
				github.com/lib/pq.
			->	The second argument is a driver-specific syntax that tells the
				driver how to access the underlying datastore. In this example,
				we're connecting to the "hello" database inside a local MySQL
				server instance.
			-> 	You should (almost) always check and handle errors returned from
				all database/sql operations. There are a few special cases that we'll
				discuss later where it doesn't make sense to do this.
			->	It is idiomatic to defer db.Close() if the sql.DB should not
				have a lifetime beyond the scope of the function.
		Perhaps counter-intuitively, sql.Open() does not establish any connections
		to the database, nor does it validate driver connection parameters. Instead,
		it simply prepares the database abstraction for later use. The first actual
		connection to the underlying datastore will be established lazily, when
		it's needed for the first time.

		Don't Open() and Close() databases frequently. Instead, create one sql.DB
		object for each distinct datastore you need to access, and keep it until
		the program is done accessing that datastore. Pass it around as needed,
		or make it available somehow globally, but keep it open. And don't Open()
		and Close() from a short-lived function. Instead, pass the sql.DB into
		that short-lived function as an argument.
	*/

	var database string

	// STEP 1: CREATE SQL STATEMENT
	sqlStatement := "show databases"

	// STEP 2: ROW SET THROUGH RUNNING SQL STATEMENT
	rows, err := db.Query(sqlStatement)
	//  If a function name includes Query, it is designed to ask a question
	// of the database, and will return a set of rows, even if it's empty.
	// Statements that don't return rows should not use Query functions;
	// they should use Exec().
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Query running")
	}
	// Note the defer
	defer rows.Close()
	// rows.Close() is a harmless no-op if it's already closed, so you can
	// call it multiple times. Notice, however, that we check the error first,
	// and only call rows.Close() if there isn't an error, in order to avoid
	// a runtime panic.
	//
	// You should always defer rows.Close(), even if you also call rows.Close()
	// explicitly at the end of the loop, which isn't a bad idea.
	//
	// Don't defer within a loop. A deferred statement doesn't get executed
	// until the function exits, so a long-running function shouldn't use it.
	// If you do, you will slowly accumulate memory. If you are repeatedly
	// querying and consuming result sets within a loop, you should explicitly
	// call rows.Close() when you're done with each result, and not use defer.

	// STEP 3: GO THROUGH THE ROWS
	for rows.Next() {
		err := rows.Scan(&database)
		// When you iterate over rows and scan them into destination
		//variables, Go performs data type conversions work for you, behind
		//the scenes. It is based on the type of the destination variable.
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(database)
	}
	err = rows.Err()
	// You should always check for an error at the end of the for rows.Next()
	// loop. If there's an error during the loop, you need to know about it.
	// Don't just assume that the loop iterates until you've processed all
	// the rows.
	if err != nil {
		log.Fatal(err)
	}

}
