package main

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "root:1q2w3e4r!@tcp(127.0.0.1:3306)/test")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Connection established")
	}
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("INSERT INTO user(id,username) values(?,?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close() // danger!
	for i := 0; i < 10; i++ {
		_, err = stmt.Exec(i, "user_"+strconv.FormatInt(int64(i), 10)) // NOTE INTEGER TO STRING CONV
		if err != nil {
			log.Fatal(err)
			tx.Rollback()
		}
	}
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}
	// stmt.Close() runs here!
}
