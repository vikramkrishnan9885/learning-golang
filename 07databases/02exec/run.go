package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "root:1q2w3e4r!@tcp(127.0.0.1:3306)/test")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Connection established")
	}

	createStmt, _ := db.Prepare("create table if not exists user (id int, username varchar(255))")
	_, err = createStmt.Exec()
	if err != nil {
		log.Fatal(err)
	}

	stmt, err := db.Prepare("INSERT INTO user(id,username) values(?,?)") // IN REAL LIFE ONLY UPSERT
	if err != nil {
		log.Fatal(err)
	}
	res, err := stmt.Exec(33, "Ray Martin")
	if err != nil {
		log.Fatal(err)
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("ID = %d, affected rows = %d \n", lastID, rowCnt)

	var numItems int
	sqlStatement := "select count(id) from user"
	result := db.QueryRow(sqlStatement).Scan(&numItems)
	if result != nil {
		fmt.Println("Bugger!")
	}
	fmt.Printf("Number of rows: %d\n", numItems)

	_, err = db.Exec("DELETE from user")
	if err != nil {
		log.Fatal(err)
	}

}
