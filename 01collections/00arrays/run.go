package main

import "fmt"

func main() {

	var a [5]int
	fmt.Println("initialize an empty array: ", a)

	a[4] = 100
	fmt.Println("set: ", a)
	fmt.Println("get: ", a[4])
	fmt.Println("len: ", len(a))

	b := [5]int{1, 2, 3, 4, 5}
	fmt.Println("declared and initiliazed array: ", b)

	fmt.Println("Working with 2D array")
	var twoDimArray [2][3]int
	for i := 0; i < 2; i++ {
		for j := 0; j < 3; j++ {
			fmt.Println(twoDimArray[i][j])
		}
	}

	fmt.Println("Let  the compiler decide the array size")
	c := [...]int{12, 78, 50}
	fmt.Println(c)

	fmt.Println("Size of the array is a part of the type")
	x := [3]int{5, 78, 8}
	// var y [5]int
	// y = x // Will not work
	var z [3]int
	z = x
	fmt.Println(z)

	fmt.Println("Arrays are value types: Arrays in Go are value types and not reference types. This means that when they are assigned to a new variable, a copy of the original array is assigned to the new variable. If changes are made to the new variable, it will not be reflected in the original array.")
	p := [...]string{"USA", "China", "India", "Germany", "France"}
	q := p // a copy of a is assigned to b
	q[0] = "Singapore"
	fmt.Println("p is ", p)
	fmt.Println("q is ", q)
}
