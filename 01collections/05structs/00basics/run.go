package main

import "fmt"

type person struct {
	name string
	age  int
}

type personAnonymousFields struct {
	string
	int
}

type address struct {
	city, state string
}
type personWAddress struct {
	name    string
	age     int
	address address
}

type name struct {
	firstName string
	lastName  string
}

func main() {
	fmt.Println("==========================================================")
	fmt.Println("Creating a struct")
	fmt.Println("==========================================================")
	fmt.Println(person{"bob", 20})

	fmt.Println("==========================================================")
	fmt.Println("Creating a struct with named field")
	fmt.Println("==========================================================")
	fmt.Println(person{name: "Alice", age: 30})

	fmt.Println("==========================================================")
	fmt.Println("Omitted fields will be zero-valued.")
	fmt.Println("==========================================================")
	fmt.Println(person{name: "Fred"})

	fmt.Println("==========================================================")
	fmt.Println("An & prefix yields a pointer to the struct.")
	fmt.Println("==========================================================")
	fmt.Println(&person{name: "Ann", age: 40}) //

	fmt.Println("==========================================================")
	fmt.Println("Access struct fields with a dot.")
	fmt.Println("==========================================================")
	s := person{name: "Sean", age: 50}
	fmt.Println(s.name)

	fmt.Println("==========================================================")
	fmt.Println("You can also use dots with struct pointers - the pointers are automatically dereferenced.")
	fmt.Println("==========================================================")
	sp := &s
	fmt.Println(sp.age)

	fmt.Println("==========================================================")
	fmt.Println("Structs are mutable.")
	fmt.Println("==========================================================")
	sp.age = 51
	fmt.Println(sp.age)

	// Anonymous struct
	emp3 := struct {
		firstName, lastName string
		age, salary         int
	}{
		firstName: "Andreah",
		lastName:  "Nikola",
		age:       31,
		salary:    5000,
	}
	fmt.Println("==========================================================")
	fmt.Println("Anonymous struct")
	fmt.Println("==========================================================")
	fmt.Println("Employee 3", emp3)

	p := personAnonymousFields{"Naveen", 50}
	fmt.Println(p)

	fmt.Println("==========================================================")
	fmt.Println("Nested Structs")
	fmt.Println("==========================================================")
	var p1 personWAddress
	p1.name = "Naveen"
	p1.age = 50
	p1.address = address{
		city:  "Chicago",
		state: "Illinois",
	}
	fmt.Println("Name:", p1.name)
	fmt.Println("Age:", p1.age)
	fmt.Println("City:", p1.address.city)
	fmt.Println("State:", p1.address.state)

	fmt.Println("==========================================================")
	fmt.Println("Equality of Structs")
	fmt.Println("==========================================================")
	name1 := name{"Steve", "Jobs"}
	name2 := name{"Steve", "Jobs"}
	if name1 == name2 {
		fmt.Println("name1 and name2 are equal")
	} else {
		fmt.Println("name1 and name2 are not equal")
	}

	name3 := name{firstName: "Steve", lastName: "Jobs"}
	name4 := name{}
	name4.firstName = "Steve"
	if name3 == name4 {
		fmt.Println("name3 and name4 are equal")
	} else {
		fmt.Println("name3 and name4 are not equal")
	}
}
