package main

import "encoding/json"
import "fmt"
import "os"

type response1 struct {
	Page   int
	Fruits []string
}
type response2 struct {
	Page   int      `json:"page"`
	Fruits []string `json:"fruits"`
}

func main() {
	fmt.Println("==========================================================")
	fmt.Println("Encoding basic data types to JSON strings. Here are some examples for atomic values.")
	fmt.Println("==========================================================")
	bolB, _ := json.Marshal(true)
	fmt.Println(string(bolB))
	intB, _ := json.Marshal(1)
	fmt.Println(string(intB))
	fltB, _ := json.Marshal(2.34)
	fmt.Println(string(fltB))
	strB, _ := json.Marshal("gopher")
	fmt.Println(string(strB))

	fmt.Println("==========================================================")
	fmt.Println("And here are some for slices and maps, which encode to JSON arrays and objects as you’d expect.")
	fmt.Println("==========================================================")
	slcD := []string{"apple", "peach", "pear"}
	slcB, _ := json.Marshal(slcD)
	fmt.Println(string(slcB))
	mapD := map[string]int{"apple": 5, "lettuce": 7}
	mapB, _ := json.Marshal(mapD)
	fmt.Println(string(mapB))

	fmt.Println("==========================================================")
	fmt.Println("JSON package can automatically encode your custom data types. It will only include exported fields in the encoded output and will by default use those names as the JSON keys.")
	fmt.Println("==========================================================")
	res1D := &response1{
		Page:   1,
		Fruits: []string{"apple", "peach", "pear"}}
	res1B, _ := json.Marshal(res1D)
	fmt.Println(string(res1B))

	fmt.Println("==========================================================")
	fmt.Println("You can use tags on struct field declarations to customize the encoded JSON key names. Check the definition of response2 above to see an example of such tags.")
	fmt.Println("==========================================================")

	res2D := &response2{
		Page:   1,
		Fruits: []string{"apple", "peach", "pear"}}
	res2B, _ := json.Marshal(res2D)
	fmt.Println(string(res2B))

	fmt.Println("==========================================================")
	fmt.Println("Now let’s look at decoding JSON data into Go values. Here’s an example for a generic data structure.")

	byt := []byte(`{"num":6.13,"strs":["a","b"]}`)
	fmt.Println("==========================================================")
	fmt.Println("We need to provide a variable where the JSON package can put the decoded data. This map[string]interface{} will hold a map of strings to arbitrary data types.")

	var dat map[string]interface{}

	fmt.Println("==========================================================")
	fmt.Println("Here’s the actual decoding, and a check for associated errors.")
	fmt.Println("==========================================================")
	if err := json.Unmarshal(byt, &dat); err != nil {
		panic(err)
	}
	fmt.Println(dat)

	fmt.Println("==========================================================")
	fmt.Println("In order to use the values in the decoded map, we’ll need to convert them to their appropriate type. For example here we convert the value in num to the expected float64 type.")
	fmt.Println("==========================================================")
	num := dat["num"].(float64)
	fmt.Println(num)

	fmt.Println("==========================================================")
	fmt.Println("Accessing nested data requires a series of conversions.")
	fmt.Println("==========================================================")
	strs := dat["strs"].([]interface{})
	str1 := strs[0].(string)
	fmt.Println(str1)

	fmt.Println("==========================================================")
	fmt.Println("We can also decode JSON into custom data types. This has the advantages of adding additional type-safety to our programs and eliminating the need for type assertions when accessing the decoded data.")
	fmt.Println("==========================================================")
	str := `{"page": 1, "fruits": ["apple", "peach"]}`
	res := response2{}
	json.Unmarshal([]byte(str), &res)
	fmt.Println(res)
	fmt.Println(res.Fruits[0])

	fmt.Println("==========================================================")
	fmt.Println("In the examples above we always used bytes and strings as intermediates between the data and JSON representation on standard out. We can also stream JSON encodings directly to os.Writers like os.Stdout or even HTTP response bodies.")
	fmt.Println("==========================================================")
	enc := json.NewEncoder(os.Stdout)
	d := map[string]int{"apple": 5, "lettuce": 7}
	enc.Encode(d)
}
