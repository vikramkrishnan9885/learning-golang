package main

import "fmt"

func printBytes(s string) {
	for i := 0; i < len(s); i++ {
		fmt.Printf("%x \n", s[i])
	}
}

func printChars(s string) {
	for i := 0; i < len(s); i++ {
		fmt.Printf("%c ", s[i])
	}
}

func printCharsRunes(s string) {
	runes := []rune(s)
	for i := 0; i < len(runes); i++ {
		fmt.Printf("%c ", runes[i])
	}
}

func printCharsAndBytes(s string) {
	// for range loop on a string
	for index, rune := range s {
		fmt.Printf("%c starts at byte %d\n", rune, index)
	}
}

func mutate(s []rune) string {
	s[0] = 'a'
	return string(s)
}

func main() {
	name := "Hello World"
	printBytes(name)
	fmt.Printf("\n")
	printChars(name)
	fmt.Printf("\n")
	printCharsRunes(name)
	fmt.Printf("\n")
	printCharsAndBytes(name)
	fmt.Printf("\n")
	name = "Señor"
	printBytes(name)
	fmt.Printf("\n")
	printChars(name)
	fmt.Printf("\n")
	printCharsRunes(name)
	fmt.Printf("\n")
	printCharsAndBytes(name)
	fmt.Printf("\n")

	fmt.Println("==========================================================")
	fmt.Println("Constructing string from slice of bytes")
	fmt.Println("==========================================================")
	byteSlice := []byte{0x43, 0x61, 0x66, 0xC3, 0xA9}
	str := string(byteSlice)
	fmt.Println(str)

	fmt.Println("==========================================================")
	fmt.Println("Constructing string from slice of decimal eq. of bytes")
	fmt.Println("==========================================================")
	byteSliceDec := []byte{67, 97, 102, 195, 169} //decimal equivalent of {'\x43', '\x61', '\x66', '\xC3', '\xA9'}
	strDec := string(byteSliceDec)
	fmt.Println(strDec)

	fmt.Println("==========================================================")
	fmt.Println("Constructing string from slice of runes")
	fmt.Println("==========================================================")
	runeSlice := []rune{0x0053, 0x0065, 0x00f1, 0x006f, 0x0072}
	strRune := string(runeSlice)
	fmt.Println(strRune)

	// To workaround this string immutability, strings are converted to a
	// slice of runes. Then that slice is mutated with whatever changes
	// needed and converted back to a new string.
	fmt.Println("==========================================================")
	fmt.Println("Modifying strings using runes")
	fmt.Println("==========================================================")
	h := "hello"
	fmt.Println(mutate([]rune(h)))
}
