package main

import (
	"bytes"
	"fmt"
	"regexp"
)

func main() {

	fmt.Println("==========================================================")
	fmt.Println("STRING MATCH")
	fmt.Println("==========================================================")
	match, _ := regexp.MatchString("p([a-z]+)ch", "peach")
	fmt.Println(match)

	fmt.Println("==========================================================")
	fmt.Println("STRING MATCH AFTER COMPILING")
	fmt.Println("==========================================================")
	r, _ := regexp.Compile("p([a-z]+)ch") // Improves perf. & can be reused
	fmt.Println(r.MatchString("peach"))

	fmt.Println("==========================================================")
	fmt.Println("FIRST MATCH")
	fmt.Println("==========================================================")
	fmt.Println(r.FindString("peach punch"))
	fmt.Println(r.FindStringIndex("peach punch"))

	fmt.Println("==========================================================")
	fmt.Println("SUBMATCHES")
	fmt.Println("==========================================================")
	//The Submatch variants include information about both the whole-pattern matches
	// and the submatches within those matches. For example this will return
	// information for both p([a-z]+)ch and ([a-z]+).
	fmt.Println(r.FindStringSubmatch("peach punch"))
	fmt.Println(r.FindStringSubmatchIndex("peach punch"))

	fmt.Println("==========================================================")
	fmt.Println("ALL MATCHES")
	fmt.Println("==========================================================")
	fmt.Println(r.FindAllString("peach punch pinch", -1))
	fmt.Println(r.FindAllStringSubmatchIndex("peach punch pinch", -1)) // Providing a non-negative integer as the second argument to these functions will limit the number of matches.
	fmt.Println(r.FindAllString("peach punch pinch", 2))

	fmt.Println("==========================================================")
	fmt.Println("BYTE MATCHING")
	fmt.Println("==========================================================")
	fmt.Println(r.Match([]byte("peach")))

	fmt.Println("==========================================================")
	fmt.Println("OTHER POINTS")
	fmt.Println("==========================================================")

	r = regexp.MustCompile("p([a-z]+)ch") // When creating constants with regular expressions you can use the MustCompile variation of Compile. A plain Compile won’t work for constants because it has 2 return values.
	fmt.Println(r)

	fmt.Println(r.ReplaceAllString("a peach", "<fruit>")) // The regexp package can also be used to replace subsets of strings with other values.

	in := []byte("a peach") // The Func variant allows you to transform matched text with a given function.
	out := r.ReplaceAllFunc(in, bytes.ToUpper)
	fmt.Println(string(out))
}
