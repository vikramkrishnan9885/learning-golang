package main

import (
	"fmt"
	"strings"
)

var p = fmt.Println // alias functions

func main() {
	p("Contains st: ", strings.Contains("test", "st"))
	p("Count of t: ", strings.Count("test", "t"))
	p("HasPrefix te: ", strings.HasPrefix("test", "te"))
	p("HasSuffix st: ", strings.HasSuffix("test", "st"))
	p("Index e: ", strings.Index("test", "e"))
	p("Join string array containing a b with '-': ", strings.Join([]string{"a", "b"}, "-"))
	p("Repeat: ", strings.Repeat("a", 5))
	p("Replace If n < 0, there is no limit on the number of replacements.:   ", strings.Replace("foo", "o", "0", -1))
	p("Replace n=1: ", strings.Replace("foo", "o", "0", 1))
	p("Split: ", strings.Split("a-b-c-d-e", "-"))
	p("ToLower: ", strings.ToLower("TEST"))
	p("ToUpper: ", strings.ToUpper("test"))
	p()

	// Not part of strings, but worth mentioning here, are the mechanisms for getting the length of a string in bytes and getting a byte by index.
	p("Len: ", len("hello"))
	p("Char:", "hello"[1])
}
