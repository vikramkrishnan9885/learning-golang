package main

import "fmt"

func main() {

	fmt.Println("================================================")
	fmt.Println("SLICES ARE TYPED ONLY BY THE ELEMENT TYPE AND NOT NUMBER OF ELEMENTS")
	fmt.Println("================================================")
	s := make([]string, 3)
	// func make([]T, len, cap) []T can be used to create a slice by passing
	// the type, length and capacity. The capacity parameter is optional and
	// defaults to the length.
	fmt.Println(s)

	fmt.Println("================================================")
	fmt.Println("ASSIGNMENT")
	fmt.Println("================================================")
	s[0] = "a"
	s[1] = "p"
	s[2] = "x"
	fmt.Println(s)
	fmt.Println(len(s))

	fmt.Println("================================================")
	fmt.Println("APPEND")
	fmt.Println("================================================")
	s = append(s, "d")
	s = append(s, "w")
	s = append(s, "t")
	fmt.Println(s)

	fmt.Println("================================================")
	fmt.Println("This slices up to (but excluding) s[5].")
	fmt.Println("================================================")
	l := s[:5]
	fmt.Println("sl2:", l)

	fmt.Println("================================================")
	fmt.Println("And this slices up from (and including) s[2].")
	fmt.Println("================================================")
	l = s[2:]
	fmt.Println("sl3:", l)

	fmt.Println("================================================")
	fmt.Println("ASSIGNING SLICES")
	fmt.Println("================================================")
	a := [5]int{76, 77, 78, 79, 80}
	var b = a[1:4] //creates a slice from a[1] to a[3]
	fmt.Println(b)

	fmt.Println("================================================")
	fmt.Println("MODIFYING A SLICE: A slice does not own any data of its own. It is just a representation of the underlying array. Any modifications done to the slice will be reflected in the underlying array")
	fmt.Println("================================================")
	darr := [...]int{57, 89, 90, 82, 100, 78, 67, 69, 59}
	dslice := darr[2:5]
	fmt.Println("array before", darr)
	for i := range dslice {
		dslice[i]++
	}
	fmt.Println("array after", darr)

	fmt.Println("================================================")
	fmt.Println("SLICE COPY")
	fmt.Println("================================================")
	c := make([]string, len(s))
	copy(c, s)
	fmt.Println("cpy:", c)
	// Memory Optimisation
	// Slices hold a reference to the underlying array. As long as the slice
	// is in memory, the array cannot be garbage collected. This might be of
	// concern when it comes to memory management. Lets assume that we have
	// a very large array and we are interested in processing only a small
	// part of it. Henceforth we create a slice from that array and start
	// processing the slice. The important thing to be noted here is that
	// the array will still be in memory since the slice references it.
	// One way to solve this problem is to use the copy function
	// func copy(dst, src []T) int to make a copy of that slice.
	// This way we can use the new slice and the original array can be
	// garbage collected.

	fmt.Println("================================================")
	fmt.Println("MULTIDIMENSIONAL ARRAYS")
	fmt.Println("================================================")
	pls := [][]string{
		{"C", "C++"},
		{"JavaScript"},
		{"Go", "Rust"},
	}
	for _, v1 := range pls {
		for _, v2 := range v1 {
			fmt.Printf("%s ", v2)
		}
		fmt.Printf("\n")
	}
}
