package main

import "fmt"

/**
Generator Pattern is used to generate a sequence of values which is used to
produce some output. This pattern is widely used to introduce parallelism into loops.
This allows the consumer of the data produced by the generator to run in parallel
when the generator function is busy computing the next value.

Generators in Go are implemented with goroutines.
The fib function passes the Fibonacci number with the help of channels,
which is then consumed in the loop to generate output.
The generator and the consumer can work concurrently (maybe in parallel) as
the logic involved in both are different.
*/

// Generator func which produces data which might be computationally expensive.
func fib(n int) chan int {
	c := make(chan int)
	go func() {
		for i, j := 0, 1; i < n; i, j = i+j, i {
			c <- i
		}
		close(c)
	}()
	return c
}

func main() {
	// fib returns the fibonacci numbers lesser than 1000
	for i := range fib(1000) {
		// Consumer which consumes the data produced by the generator, which further does some extra computations
		v := i * i
		fmt.Println(v)
	}
}
