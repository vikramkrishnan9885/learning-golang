package main

import (
	"fmt"
)

func calcSquares(number int, squareop chan int) {
	sum := 0
	for number != 0 {
		digit := number % 10
		sum += digit * digit
		number /= 10
	}
	// LAST STEP OF ANY GO ROUTINE WITH CHANNELS SHOULD BE WRITING TO THE CHANNEL
	// CHANNEL IS AN INPUT TO THE GO ROUTINE
	squareop <- sum
}

func calcCubes(number int, cubeop chan int) {
	sum := 0
	for number != 0 {
		digit := number % 10
		sum += digit * digit * digit
		number /= 10
	}
	cubeop <- sum
}

func main() {
	number := 589
	// STEP 1: CREATE CHANNELS
	sqrch := make(chan int)
	cubech := make(chan int)

	// STEP 2: CALL GO ROUTINES
	go calcSquares(number, sqrch)
	go calcCubes(number, cubech)

	// STEP 3: GET RESULTS
	squares, cubes := <-sqrch, <-cubech

	// STEP 4: FURTHER PROC
	fmt.Println("Final output", squares+cubes)
}
