package main

import "fmt"

func sendData(sendch chan<- int) {
	sendch <- 10
}

func main() {
	// UNDERSTANDING CHANNEL CONVERSION
	// sendch := make(chan<- int)
	// In the above line, we create send only channel sendch.
	// chan<- int denotes a send only channel as the arrow is pointing to chan.
	// If we try to receive data from a send only channel, then we get an error as
	// it is not allowed and when the program is run, the compiler will complain stating,
	// invalid operation: <-sendch (receive from send-only type chan<- int)

	// All is well but what is the point of writing to a send only channel
	// if it cannot be read from!

	// This is where channel conversion comes into use.
	// It is possible to convert a bidirectional channel to a send only or
	// receive only channel but not the vice versa.

	// Below instead, a bidirectional channel chnl is created.
	// It is passed as a parameter to the sendData Goroutine.
	// The sendData function converts this channel to a send only channel
	//  in the parameter sendch chan<- int.
	// So now the channel is send only inside the sendData Goroutine
	// but it's bidirectional in the main Goroutine.
	// This program will print 10 as the output.

	chnl := make(chan int)
	go sendData(chnl)
	fmt.Println(<-chnl)
}
