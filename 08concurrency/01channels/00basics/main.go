package main

import "fmt"

/*
WHAT ARE CHANNELS?
Channels can be thought as pipes using which Goroutines communicate.
Similar to how water flows from one end to another in a pipe,
data can be sent from one end and received from the another end using channels.

DECLARING CHANNELS
Each channel has a type associated with it.
This type is the type of data that the channel is allowed to transport.
 No other type is allowed to be transported using the channel.
*/

func main() {
	// CREATE A CHANNEL
	//a := make(chan int)
	/*
		THIS IS EQUIVALENT TO:
		var a chan int
		if a == nil {
			fmt.Println("channel a is nil, going to define it")
			a = make(chan int)
			fmt.Printf("Type of a is %T", a)
		}

		SEND AND RECEIVE FROM CHANNEL
		data := <- a // read from channel a
		a <- data // write to channel a

		Sends and receives to a channel are blocking by default.
		When a data is sent to a channel, the control is blocked in the send statement
		until some other Goroutine reads from that channel.
		Similarly when data is read from a channel, the read is blocked
		until some Goroutine writes data to that channel.

		This property of channels is what helps Goroutines communicate effectively
		without the use of explicit locks or conditional variables
		that are quite common in other programming languages.
	*/

	// USING CHANNELS WITH GO ROUTINES: YOUR GO ROUTINE SHOULD HAVE CHANNEL AS INPUT
	// (AND AS AN OUTPUT TOO TO GET MAX CONCURRENCY).
	// YOU CAN SPICE IT UP BY ADDING CONTEXT VARIABLES AS WELL
	done := make(chan bool)
	go hello(done)
	<-done
	fmt.Println("main function")

}

func hello(done chan bool) {
	fmt.Println("Hello world goroutine")
	done <- true
}

/*
DEADLOCK
==========
One important factor to consider while using channels is deadlock.
If a Goroutine is sending data on a channel,
then it is expected that some other Goroutine should be receiving the data.
If this does not happen, then the program will panic at runtime with Deadlock.

Similarly if a Goroutine is waiting to receive data from a channel,
then some other Goroutine is expected to write data on that channel,
else the program will panic.
*/

// SUPERIMP: https://www.ardanlabs.com/blog/2017/10/the-behavior-of-channels.html
