package main

/*
A WaitGroup is used to wait for a collection of Goroutines to finish executing.
The control is blocked until all Goroutines finish executing.
Lets say we have 3 concurrently executing Goroutines spawned from the main Goroutine.
The main Goroutines needs to wait for the 3 other Goroutines to finish before terminating.
This can be accomplished using WaitGroup.

The way WaitGroup works is by using a counter.
When we call Add on the WaitGroup and pass it an int,
the WaitGroup's counter is incremented by the value passed to Add.
The way to decrement the counter is by calling Done() method on the WaitGroup.
The Wait() method blocks the Goroutine in which it's called until the counter becomes zero.
*/
import (
	"fmt"
	"sync"
	"time"
)

func process(i int, wg *sync.WaitGroup) {
	fmt.Println("started Goroutine ", i)
	time.Sleep(2 * time.Second)
	fmt.Printf("Goroutine %d ended\n", i)
	// The counter is decremented by the call to wg.Done in the process Goroutine.
	wg.Done()
}

func main() {
	no := 3
	var wg sync.WaitGroup
	for i := 0; i < no; i++ {
		// we call wg.Add(1) inside the for loop which iterates 3 times.
		// The for loop also spawns 3 process Goroutines and
		// then wg.Wait() called below OUTSIDE THE LOOP that makes the main Goroutine to wait
		// until the counter becomes zero.
		wg.Add(1)

		// It is important to pass the address of wg.
		// If the address is not passed, then each Goroutine will have its own copy of the WaitGroup
		// and main will not be notified when they finish executing.
		go process(i, &wg)
	}
	// Once all the 3 spawned Goroutines finish their execution,
	// that is once wg.Done() has been called three times,
	// the counter will become zero, and the main Goroutine will be unblocked.
	wg.Wait()
	fmt.Println("All go routines finished executing")
}

// Add, go, Wait, Done
// Add, go inside loop in main
// Wait outside loop in main
// Done inside go routine
