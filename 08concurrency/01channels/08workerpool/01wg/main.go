package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

/*
One of the important uses of buffered channel is the implementation of worker pool.

In general, a worker pool is a collection of threads
which are waiting for tasks to be assigned to them. Once they finish the task assigned,
they make themselves available again for the next task.

We will implement worker pool using buffered channels.
Our worker pool will carry out the task of finding the sum of a digits of the input number.
For example if 234 is passed, the output would be 9 (2 + 3 + 4).
The input to the worker pool will be list of pseudo random integers.

The following are the core functionalities of our worker pool

1. Creation of a pool of Goroutines which listen on an input buffered channel waiting for jobs to be assigned
2. Addition of jobs to the input buffered channel
3. Writing results to an output buffered channel after job completion
4. Read and print results from the output buffered channel
*/

// PATTERN FOR ALL SUCH TASKS:
// 1. JOB STRUCT - TASK METADATA + DATA FOR TASK
// 2. OUTPUT STRUCT (ONE FIELD CONTAINS RESULTS, OTHER WITH METADATA)
// 3. CHANNELS FOR INPUT AND OUTPUT
// 4. TRANSFORMATION FUNCTION
// 5. WORKER WRAPPER AROUND THE TRANSFORMATION FUNCTION - THIS IS THE BIT THAT HANDLES WG.DONE
// 5a. WORKER INPUTS: WG, INPUT CHAN, OUTPUT CHAN (DON'T CREATE IT THE WAY IT IS HEAR WITH GLOBAL VARS)
// 6. CREATE WORKER POOL - ADD, GO, WAIT (NUM WORKERS AND INPUT AND OUTPUT CHAN AS INPUTS)
// 7. CREATE (ALLOCATE) JOBS (EXTRACT) - INPUTS ARE JOBS, INPUT CHAN
// 8. EXTRACT RESULTS(LOAD) - INPUTS ARE OUTPUT CHAN

// The first step will be creation of the structs representing the job and the result.
type Job struct {
	id       int
	randomno int
}
type Result struct {
	job Job
	// SAVING THE RESULTS IN A STRUCT
	sumofdigits int
}

// The next step is to create the buffered channels for receiving the jobs
// and writing the output.
// Worker Goroutines listen for new tasks on the jobs buffered channel.
// Once a task is complete, the result is written to the results buffered channel.
var jobs = make(chan Job, 10)
var results = make(chan Result, 10)

// The digits function below does the actual job of finding the sum of the individual digits
// of an integer and returning it.
// We will add a sleep of 2 seconds to this function just to simulate the fact that
// it takes some time for this function to calculate the result.
func digits(number int) int {
	sum := 0
	no := number
	for no != 0 {
		digit := no % 10
		sum += digit
		no /= 10
	}
	time.Sleep(2 * time.Second)
	return sum
}

// Next we will write a function which creates a worker Goroutine.
// The above function creates a worker which reads from the jobs channel,
// creates a Result struct using the current job and
// the return value of the digits function and
// then writes the result to the results buffered channel.
// This function takes a WaitGroup wg as parameter on which it will call the Done() method
// when all jobs have been completed.
func worker(wg *sync.WaitGroup) {
	for job := range jobs {
		output := Result{job, digits(job.randomno)}
		results <- output
	}
	wg.Done()
}

func createWorkerPool(noOfWorkers int) {
	var wg sync.WaitGroup
	for i := 0; i < noOfWorkers; i++ {
		wg.Add(1)
		go worker(&wg)
	}
	wg.Wait()
	close(results)
}

// The allocate function above takes the number of jobs to be created as input parameter,
// generates pseudo random numbers with a maximum value of 998,
//  creates Job struct using the random number and the for loop counter i as the id
// and then writes them to the jobs channel. It closes the jobs channel after writing all jobs.
func allocate(noOfJobs int) {
	for i := 0; i < noOfJobs; i++ {
		randomno := rand.Intn(999)
		job := Job{i, randomno}
		jobs <- job
	}
	close(jobs)
}

// The result function reads the results channel and
// prints the job id, input random no and the sum of digits of the random no.
// The result function also takes a done channel as parameter to which
// it writes to once it has printed all the results.
func result(done chan bool) {
	for result := range results {
		fmt.Printf("Job id %d, input random no %d , sum of digits %d\n", result.job.id, result.job.randomno, result.sumofdigits)
	}
	done <- true
}

func main() {
	startTime := time.Now()

	noOfJobs := 100
	go allocate(noOfJobs)

	done := make(chan bool)
	go result(done)

	noOfWorkers := 10
	createWorkerPool(noOfWorkers)

	<-done

	endTime := time.Now()
	diff := endTime.Sub(startTime)
	fmt.Println("total time taken ", diff.Seconds(), "seconds")
}
