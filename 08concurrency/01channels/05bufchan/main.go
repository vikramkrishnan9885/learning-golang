package main

import (
	"fmt"
	"time"
)

func main() {

	// create a buffered channel
	// with a capacity of 2.
	ch := make(chan string, 2)
	ch <- "geeksforgeeks"
	ch <- "geeksforgeeks world"
	fmt.Println(<-ch)
	fmt.Println(<-ch)

	// creates capacity of 2
	ch2 := make(chan int, 2)
	go write(ch2)
	time.Sleep(2 * time.Second)
	for v := range ch2 {
		fmt.Println("read value", v, "from ch2")
		time.Sleep(2 * time.Second)

	}

	/*
		LENGTH VS CAPACITY
		The capacity of a buffered channel is the number of values that
		the channel can hold. This is the value we specify when creating
		the buffered channel using the make function.

		The length of the buffered channel is the number of elements currently
		queued in it.
	*/
	ch3 := make(chan string, 3)
	ch3 <- "naveen"
	ch3 <- "paul"
	fmt.Println("capacity is", cap(ch3))
	fmt.Println("length is", len(ch3))
	fmt.Println("read value", <-ch3)
	fmt.Println("capacity is", cap(ch3))
	fmt.Println("new length is", len(ch3))
}

func write(ch2 chan int) {
	for i := 0; i < 4; i++ {
		ch2 <- i
		fmt.Println("successfully wrote", i, "to ch2")
	}
	close(ch2)
	/*
		https://stackoverflow.com/questions/8593645/is-it-ok-to-leave-a-channel-open#:~:text=It's%20only%20necessary%20to%20close,all%20data%20have%20been%20sent.&text=%22One%20general%20principle%20of%20using,channel%20has%20multiple%20concurrent%20senders.%22

		Yes, it is ok to keep a channel open.
		As the go programming language book stated:
		You needn't close every channel when you've finished with it.
		It's only necessary to close a channel when it is important to tell the
		receiving goroutines that all data have been sent.
		A channel that the garbage collector determinies to be unreachable
		will have its resources reclaimed whether or not it is closed.
		(Don't confuse this with the close operation for open files.
		It is important to call the Close method on every file when you've finished with it.)

		Yes, it's OK to leave the channel open, and in fact it is typical.
		A channel being open does not constitute a reference to the channel object,
		and so does not keep it from being garbage collected.
	*/
}

/*
Deadlocks in Buffered Golang Channel

package main

import (
    "fmt"
)

func main() {
    ch := make(chan string, 2)
    ch <- "geeksforgeeks"
    ch <- "hello"
    ch <- "geeks"
    fmt.Println(<-ch)
    fmt.Println(<-ch)
}

Here , in above code the write is blocked since the channel has exceeded its capacity and program reaches deadlock situation
*/
