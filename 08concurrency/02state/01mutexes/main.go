package main

import (
	"fmt"
	"sync"
)

var x = 0

func increment(wg *sync.WaitGroup, m *sync.Mutex) {
	m.Lock()
	x = x + 1
	m.Unlock()
	wg.Done()
}
func increment2(wg *sync.WaitGroup, ch chan bool) {
	ch <- true
	x = x + 1
	<-ch
	wg.Done()
}
func main() {
	// Solving the race condition using mutex
	// Mutex is available in the sync package.
	// There are two methods defined on Mutex namely Lock and Unlock.
	// Any code that is present between a call to Lock and Unlock
	// will be executed by only one Goroutine, thus avoiding race condition.

	// If one Goroutine already holds the lock and if a new Goroutine
	// is trying to acquire a lock, the new Goroutine will be blocked
	// until the mutex is unlocked.
	var w sync.WaitGroup
	var m sync.Mutex
	for i := 0; i < 1000; i++ {
		w.Add(1)
		go increment(&w, &m)
	}
	w.Wait()
	fmt.Println("final value of x", x)

	// Solving the race condition using channel
	var w2 sync.WaitGroup
	ch := make(chan bool, 1)
	for i := 0; i < 1000; i++ {
		w2.Add(1)
		go increment2(&w2, ch)
	}
	w2.Wait()
	fmt.Println("final value of x", x)
}
