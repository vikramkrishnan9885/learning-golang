package main

import "fmt"

type Describer interface {
	Describe()
}

type Person struct {
	name string
	age  int
}

func (p Person) Describe() {
	fmt.Printf("%s is %d years old\n", p.name, p.age)
}

type Address struct {
	state   string
	country string
}

func (a *Address) Describe() {
	fmt.Printf("State %s is in Country %s", a.state, a.country)
}

func main() {
	/*
		As we have already learnt during our discussion about methods,
		methods with value receivers accept both pointer and value receivers.
		It is legal to call a value method on anything which is a value
		or whose value can be dereferenced.
	*/
	var d1 Describer
	p1 := Person{"Sam", 25}
	d1 = p1
	d1.Describe()
	p2 := Person{"James", 32}
	d1 = &p2
	d1.Describe()

	var d2 Describer
	a := Address{"Washington", "USA"}

	/* compilation error if the following line is
	   uncommented
	   cannot use a (type Address) as type Describer
	   in assignment: Address does not implement
	   Describer (Describe method has pointer
	   receiver)

	   This will definitely surprise you since we learnt earlier that methods
	   with pointer receivers will accept both pointer and value receivers.
	   Then why isn't the code working.

	   The reason is that it is legal to call a pointer-valued method on anything
	   that is already a pointer or whose address can be taken.
	   The concrete value stored in an interface is not addressable
	   and hence it is not possible for the compiler to automatically take
	   the address of a and hence this code fails.
	*/
	//d2 = a

	d2 = &a //This works since Describer interface
	//is implemented by Address pointer in line 22
	d2.Describe()
}

// https://www.ardanlabs.com/blog/2017/06/design-philosophy-on-data-and-semantics.html
// https://medium.com/a-journey-with-go/go-should-i-use-a-pointer-instead-of-a-copy-of-my-struct-44b43b104963
// BASIC RULE
// PRIMITIVES AND COLLECTIONS USE VALUE
// USER DEFINED TYPES:
// RESOURCES - USE POINTERS
// VALUE TYPES - USE VALUES

/*
Passing by value often is cheaper
Even though Go looks a bit like C, its compiler works differently.
And C analogy does not always work with Go.
Passing by value in Go may be significantly cheaper than passing by pointer.
This happens because Go uses escape analysis to determine if variable can be safely allocated on function’s stack frame,
which could be much cheaper then allocating variable on the heap.
Passing by value simplifies escape analysis in Go and gives variable a better chance to be allocated on the stack.

Bottom line
Sometimes the choice of how to pass variable is predetermined by variable type or its usage.
Otherwise, it’s highly recommended to pass variables by value.
*/

/*
Note that pointers are not safe for concurrency;
hence you need to handle it yourself using synchronous mechanism such as channels,
or the use of atomic or sync builtin packages.

If you don't want to share a value, use value receiver.
Value receivers are safe for concurrent access.
*/

/*
STATE AND METHODS DEFINE POINTER VS VALUE CHOICE

Use the same receiver type for all your methods. This isn't always feasible, but try to.
Methods defines a behavior of a type; if the method uses a state (updates / mutates) use pointer receiver.
If a method don't mutate state, use value receiver.
Functions operates on values; functions should not depend on the state of a type.
*/

// https://github.com/golang/go/wiki/CodeReviewComments#receiver-type
