package main

import (
	"fmt"
)

// Type assertion
func assert(i interface{}) {
	s := i.(int) //get the underlying int value from i
	fmt.Println(s)
}

// Type switching
func findType(i interface{}) {
	switch i.(type) {
	case string:
		fmt.Printf("I am a string and my value is %s\n", i.(string))
	case int:
		fmt.Printf("I am an int and my value is %d\n", i.(int))
	default:
		fmt.Printf("Unknown type\n")
	}
}

// It is also possible to compare a type to an interface. If we have a type
// and if that type implements an interface, it is possible to compare this
// type with the interface it implements.

type describer interface {
	Describe()
}
type person struct {
	name string
	age  int
}

func (p person) Describe() {
	fmt.Printf("%s is %d years old \n", p.name, p.age)
}

func findType2(i interface{}) {
	switch v := i.(type) {
	case describer:
		v.Describe()
	default:
		fmt.Printf("unknown type\n")
	}
}

func main() {
	var s interface{} = 56
	assert(s)

	fmt.Println("Type Switching")
	findType("Naveen")
	findType(77)
	findType(89.98)

	findType2("Naveen")
	p := person{
		name: "Naveen R",
		age:  25,
	}
	findType2(p)
}
