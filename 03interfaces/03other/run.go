package main

import (
	"fmt"
)

// Implementing multiple interfaces

type SalaryCalculator interface {
	DisplaySalary()
}

type LeaveCalculator interface {
	CalculateLeavesLeft() int
}

type Employee struct {
	firstName   string
	lastName    string
	basicPay    int
	pf          int
	totalLeaves int
	leavesTaken int
}

func (e Employee) DisplaySalary() {
	fmt.Printf("%s %s has salary $%d", e.firstName, e.lastName, (e.basicPay + e.pf))
}

func (e Employee) CalculateLeavesLeft() int {
	return e.totalLeaves - e.leavesTaken
}

// Embedding interfaces
// Although go does not offer inheritance, it is possible to create a new
// interfaces by embedding other interfaces.
type EmployeeOperations interface {
	SalaryCalculator
	LeaveCalculator
}

func main() {
	fmt.Println("===================================================")
	fmt.Println("MULTIPLE INTERFACES")
	fmt.Println("===================================================")
	e := Employee{
		firstName:   "Vikram",
		lastName:    "Krishnan",
		basicPay:    5000,
		pf:          200,
		totalLeaves: 30,
		leavesTaken: 5,
	}
	var s SalaryCalculator = e
	s.DisplaySalary()
	var l LeaveCalculator = e
	fmt.Println("\nLeaves left =", l.CalculateLeavesLeft())

	fmt.Println("===================================================")
	fmt.Println("EMBEDDING INTERFACES")
	fmt.Println("===================================================")
	e2 := Employee{
		firstName:   "Vikram",
		lastName:    "Krishnan",
		basicPay:    5000,
		pf:          200,
		totalLeaves: 30,
		leavesTaken: 5,
	}
	var empOp EmployeeOperations = e2
	empOp.DisplaySalary()
	fmt.Println("\nLeaves left =", empOp.CalculateLeavesLeft())
}
