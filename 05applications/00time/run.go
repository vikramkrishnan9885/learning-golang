package main

import "fmt"
import "time"

func main() {
	p := fmt.Println

	p("=========================================================")
	p("We’ll start by getting the current time.")
	p("=========================================================")
	now := time.Now()
	p(now)

	p("=========================================================")
	p("You can build a time struct by providing the year, month, day, etc. Times are always associated with a Location, i.e. time zone.")
	p("=========================================================")
	then := time.Date(
		2009, 11, 17, 20, 34, 58, 651387237, time.UTC)
	p(then)

	p("=========================================================")
	p("You can extract the various components of the time value as expected.")
	p("=========================================================")
	p(then.Year())
	p(then.Month())
	p(then.Day())
	p(then.Hour())
	p(then.Minute())
	p(then.Second())
	p(then.Nanosecond())
	p(then.Location())

	p("=========================================================")
	p("The Monday-Sunday Weekday is also available.")
	p("=========================================================")
	p(then.Weekday())

	p("=========================================================")
	p("These methods compare two times, testing if the first occurs before, after, or at the same time as the second, respectively.")
	p("=========================================================")
	p(then.Before(now))
	p(then.After(now))
	p(then.Equal(now))

	p("=========================================================")
	p("The Sub methods returns a Duration representing the interval between two times.")
	p("=========================================================")
	diff := now.Sub(then)
	p(diff)

	p("=========================================================")
	p("We can compute the length of the duration in various units.")
	p("=========================================================")
	p(diff.Hours())
	p(diff.Minutes())
	p(diff.Seconds())
	p(diff.Nanoseconds())

	p("=========================================================")
	p("You can use Add to advance a time by a given duration, or with a - to move backwards by a duration.")
	p("=========================================================")
	p(then.Add(diff))
	p(then.Add(-diff))

	p("=========================================================")
	p("Use time.Now with Unix or UnixNano to get elapsed time since the Unix epoch in seconds or nanoseconds, respectively.")
	p("=========================================================")
	now = time.Now()
	secs := now.Unix()
	nanos := now.UnixNano()
	fmt.Println(now)

	p("=========================================================")
	p("Note that there is no UnixMillis, so to get the milliseconds since epoch you’ll need to manually divide from nanoseconds.")
	p("=========================================================")
	millis := nanos / 1000000
	fmt.Println(secs)
	fmt.Println(millis)
	fmt.Println(nanos)

	p("=========================================================")
	p("You can also convert integer seconds or nanoseconds since the epoch into the corresponding time.")
	p("=========================================================")
	fmt.Println(time.Unix(secs, 0))
	fmt.Println(time.Unix(0, nanos))

	t := time.Now()
	p(t.Format(time.RFC3339))

	p("=========================================================")
	p("Time parsing uses the same layout values as Format.")
	p("=========================================================")
	t1, e := time.Parse(
		time.RFC3339,
		"2012-11-01T22:08:41+00:00")
	p(t1)

	p("=========================================================")
	p("Format and Parse use example-based layouts. Usually you’ll use a constant from time for these layouts, but you can also supply custom layouts. Layouts must use the reference time Mon Jan 2 15:04:05 MST 2006 to show the pattern with which to format/parse a given time/string. The example time must be exactly as shown: the year 2006, 15 for the hour, Monday for the day of the week, etc.")
	p("=========================================================")
	p(t.Format("3:04PM"))
	p(t.Format("Mon Jan _2 15:04:05 2006"))
	p(t.Format("2006-01-02T15:04:05.999999-07:00"))
	form := "3 04 PM"
	t2, e := time.Parse(form, "8 41 PM")
	p(t2)

	p("=========================================================")
	p("For purely numeric representations you can also use standard string formatting with the extracted components of the time value.")
	p("=========================================================")
	fmt.Printf("%d-%02d-%02dT%02d:%02d:%02d-00:00\n",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	p("=========================================================")
	p("Parse will return an error on malformed input explaining the parsing problem.")
	p("=========================================================")
	ansic := "Mon Jan _2 15:04:05 2006"
	_, e = time.Parse(ansic, "8:41PM")
	p(e)
}
