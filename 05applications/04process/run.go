package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
)

func main() {

	// SPAWNING PROCESSES

	// We’ll start with a simple command that takes no arguments or input
	// and just prints something to stdout. The exec.Command helper creates
	// an object to represent this external process.

	dateCmd := exec.Command("date")

	// .Output is another helper that handles the common case of running a
	// command, waiting for it to finish, and collecting its output. If
	// there were no errors, dateOut will hold bytes with the date info.

	dateOut, err := dateCmd.Output()
	if err != nil {
		panic(err)
	}
	fmt.Println("> date")
	fmt.Println(string(dateOut))

	// Next we’ll look at a slightly more involved case where we pipe data
	// to the external process on its stdin and collect the results from its stdout.

	grepCmd := exec.Command("grep", "hello")

	// Here we explicitly grab input/output pipes, start the process,
	// write some input to it, read the resulting output, and finally wait
	// for the process to exit.

	grepIn, _ := grepCmd.StdinPipe()
	grepOut, _ := grepCmd.StdoutPipe()
	grepCmd.Start()
	grepIn.Write([]byte("hello grep\ngoodbye grep"))
	grepIn.Close()
	grepBytes, _ := ioutil.ReadAll(grepOut)
	grepCmd.Wait()

	// We ommited error checks in the above example, but you could use
	// the usual if err != nil pattern for all of them. We also only
	// collect the StdoutPipe results, but you could collect the StderrPipe
	// in exactly the same way.

	fmt.Println("> grep hello")
	fmt.Println(string(grepBytes))

	// Note that when spawning commands we need to provide an explicitly
	// delineated command and argument array, vs. being able to just pass
	// in one command-line string. If you want to spawn a full command with
	// a string, you can use bash’s -c option:

	lsCmd := exec.Command("bash", "-c", "ls -a -l -h")
	lsOut, err := lsCmd.Output()
	if err != nil {
		panic(err)
	}
	fmt.Println("> ls -a -l -h")
	fmt.Println(string(lsOut))

	// EXEC'ING PROCESSES
	// For our example we’ll exec ls. Go requires an absolute path to the
	// binary we want to execute, so we’ll use exec.LookPath to find it
	// (probably /bin/ls).

	binary, lookErr := exec.LookPath("ls")
	if lookErr != nil {
		panic(lookErr)
	}

	// Exec requires arguments in slice form (as apposed to one big string).
	// We'll give ls a few common arguments. Note that the first argument should be the program name.

	args := []string{"ls", "-a", "-l", "-h"}

	// Exec also needs a set of environment variables to use.
	// Here we just provide our current environment.
	env := os.Environ()

	// Here's the actual syscall.Exec call. If this call is successful,
	//the execution of our process will end here and be replaced by the /bin/ls -a -l -h process.
	// If there is an error we’ll get a return value.
	execErr := syscall.Exec(binary, args, env)
	if execErr != nil {
		panic(execErr)
	}

	// SIGNALS

	// Go signal notification works by sending os.Signal values on a channel.
	// We'll create a channel to receive these notifications (we'll also make one to notify us when the program can exit).

	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	// signal.Notify registers the given channel to receive notifications of
	// the specified signals.

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// This goroutine executes a blocking receive for signals. When it gets
	// one it’ll print it out and then notify the program that it can finish.

	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		done <- true
	}()

	// The program will wait here until it gets the expected signal
	// (as indicated by the goroutine above sending a value on done) and then exit.

	fmt.Println("awaiting signal")
	<-done
	fmt.Println("exiting")
}
