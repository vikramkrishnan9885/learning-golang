// The filepath package provides functions to parse and construct file paths in a way that is portable between operating systems; dir/file on Linux vs. dir\file on Windows, for example.

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	f := fmt.Println
	f("***********************************************************************")
	f("Join should be used to construct paths in a portable way. It takes any number of arguments and constructs a hierarchical path from them.")
	f("***********************************************************************")
	p := filepath.Join("dir1", "dir2", "filename")
	fmt.Println("p:", p)

	f("***********************************************************************")
	f("You should always use Join instead of concatenating /s or \\s manually. In addition to providing portability, Join will also normalize paths by removing superfluous separators and directory changes.")
	f("***********************************************************************")
	fmt.Println(filepath.Join("dir1//", "filename"))
	fmt.Println(filepath.Join("dir1/../dir1", "filename"))

	f("***********************************************************************")
	f("Dir and Base can be used to split a path to the directory and the file. Alternatively, Split will return both in the same call.")
	f("***********************************************************************")
	fmt.Println("Dir(p):", filepath.Dir(p))
	fmt.Println("Base(p):", filepath.Base(p))

	f("***********************************************************************")
	f("We can check whether a path is absolute.")
	f("***********************************************************************")
	fmt.Println(filepath.IsAbs("dir/file"))
	fmt.Println(filepath.IsAbs("/dir/file"))
	filename := "config.json"

	f("***********************************************************************")
	f("Some file names have extensions following a dot. We can split the extension out of such names with Ext.")
	f("***********************************************************************")
	ext := filepath.Ext(filename)
	fmt.Println(ext)

	f("***********************************************************************")
	f("To find the file’s name with the extension removed, use strings.TrimSuffix.")
	f("***********************************************************************")
	fmt.Println(strings.TrimSuffix(filename, ext))

	f("***********************************************************************")
	f("Rel finds a relative path between a base and a target. It returns an error if the target cannot be made relative to base.")
	f("***********************************************************************")
	rel, err := filepath.Rel("a/b", "a/b/t/file")
	if err != nil {
		panic(err)
	}
	fmt.Println(rel)
	rel, err = filepath.Rel("a/b", "a/c/t/file")
	if err != nil {
		panic(err)
	}
	fmt.Println(rel)

	f("***********************************************************************")
	f("Create a new sub-directory in the current working directory.")
	f("***********************************************************************")
	err = os.Mkdir("subdir", 0755)
	check(err)

	f("***********************************************************************")
	f("When creating temporary directories, it’s good practice to defer their removal. os.RemoveAll will delete a whole directory tree (similarly to rm -rf).")
	f("***********************************************************************")
	defer os.RemoveAll("subdir")

	f("***********************************************************************")
	f("Helper function to create a new empty file.")
	f("***********************************************************************")
	createEmptyFile := func(name string) {
		d := []byte("")
		check(ioutil.WriteFile(name, d, 0644))
	}
	createEmptyFile("subdir/file1")

	f("***********************************************************************")
	f("We can create a hierarchy of directories, including parents with MkdirAll. This is similar to the command-line mkdir -p.")
	f("***********************************************************************")
	err = os.MkdirAll("subdir/parent/child", 0755)
	check(err)
	createEmptyFile("subdir/parent/file2")
	createEmptyFile("subdir/parent/file3")
	createEmptyFile("subdir/parent/child/file4")

	f("***********************************************************************")
	f("ReadDir lists directory contents, returning a slice of os.FileInfo objects.")
	f("***********************************************************************")
	c, err := ioutil.ReadDir("subdir/parent")
	check(err)
	fmt.Println("Listing subdir/parent")
	for _, entry := range c {
		fmt.Println(" ", entry.Name(), entry.IsDir())
	}

	f("***********************************************************************")
	f("Chdir lets us change the current working directory, similarly to cd.")
	f("***********************************************************************")
	err = os.Chdir("subdir/parent/child")
	check(err)

	f("***********************************************************************")
	f("Now we’ll see the contents of subdir/parent/child when listing the current directory.")
	f("***********************************************************************")
	c, err = ioutil.ReadDir(".")
	check(err)
	fmt.Println("Listing subdir/parent/child")
	for _, entry := range c {
		fmt.Println(" ", entry.Name(), entry.IsDir())
	}

	f("***********************************************************************")
	f("cd back to where we started.")
	f("***********************************************************************")
	err = os.Chdir("../../..")
	check(err)
	f("***********************************************************************")
	f("We can also visit a directory recursively, including all its sub-directories. Walk accepts a callback function to handle every file or directory visited.")
	f("***********************************************************************")
	fmt.Println("Visiting subdir")
	err = filepath.Walk("subdir", visit)

	f("***********************************************************************")
	f("Throughout program execution, we often want to create data that isn’t needed after the program exits. Temporary files and directories are useful for this purpose since they don’t pollute the file system over time.")
	f("***********************************************************************")
	f("The easiest way to create a temporary file is by calling ioutil.TempFile. It creates a file and opens it for reading and writing. We provide \"\" as the first argument, so ioutil.TempFile will create the file in the default location for our OS.")
	f("***********************************************************************")
	x, err := ioutil.TempFile("", "sample")
	check(err)

	f("***********************************************************************")
	f("Display the name of the temporary file. On Unix-based OSes the directory will likely be /tmp. The file name starts with the prefix given as the second argument to ioutil.TempFile and the rest is chosen automatically to ensure that concurrent calls will always create different file names.")
	f("***********************************************************************")
	fmt.Println("Temp file name:", x.Name())
	f("***********************************************************************")
	f("Clean up the file after we’re done. The OS is likely to clean up temporary files by itself after some time, but it’s good practice to do this explicitly.")
	f("***********************************************************************")
	defer os.Remove(x.Name())
	f("***********************************************************************")
	f("We can write some data to the file.")
	f("***********************************************************************")
	_, err = x.Write([]byte{1, 2, 3, 4})
	check(err)
	f("***********************************************************************")
	f("If we intend to write many temporary files, we may prefer to create a temporary directory. ioutil.TempDir’s arguments are the same as TempFile’s, but it returns a directory name rather than an open file.")
	f("***********************************************************************")
	dname, err := ioutil.TempDir("", "sampledir")
	fmt.Println("Temp dir name:", dname)
	defer os.RemoveAll(dname)
	f("***********************************************************************")
	f("Now we can synthesize temporary file names by prefixing them with our temporary directory.")
	f("***********************************************************************")
	fname := filepath.Join(dname, "file1")
	err = ioutil.WriteFile(fname, []byte{1, 2}, 0666)
	check(err)
}

// visit is called for every file or directory found recursively by
// filepath.Walk.

func visit(p string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}
	fmt.Println(" ", p, info.IsDir())
	return nil
}
