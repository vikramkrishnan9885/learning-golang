package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

// Reading files requires checking most calls for errors.
// This helper will streamline our error checks below.
func check(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {

	p := fmt.Println

	p("=========================================================")
	p("Perhaps the most basic file reading task is slurping a file’s entire contents into memory.")
	p("=========================================================")
	dat, err := ioutil.ReadFile("test.txt")
	check(err)
	fmt.Print(string(dat))

	p("=========================================================")
	p("You’ll often want more control over how and what parts of a file are read. For these tasks, start by Opening a file to obtain an os.File value.")
	p("=========================================================")
	f, err := os.Open("test.txt")
	check(err)

	p("=========================================================")
	p("Read some bytes from the beginning of the file. Allow up to 5 to be read but also note how many actually were read.")
	p("=========================================================")
	b1 := make([]byte, 5)
	n1, err := f.Read(b1)
	check(err)
	fmt.Printf("%d bytes: %s\n", n1, string(b1[:n1]))

	p("=========================================================")
	p("You can also Seek to a known location in the file and Read from there.")
	p("=========================================================")
	o2, err := f.Seek(6, 0)
	check(err)
	b2 := make([]byte, 2)
	n2, err := f.Read(b2)
	check(err)
	fmt.Printf("%d bytes @ %d: ", n2, o2)
	fmt.Printf("%v\n", string(b2[:n2]))

	p("=========================================================")
	p("The io package provides some functions that may be helpful for file reading. For example, reads like the ones above can be more robustly implemented with ReadAtLeast.")
	p("=========================================================")
	o3, err := f.Seek(6, 0)
	check(err)
	b3 := make([]byte, 2)
	n3, err := io.ReadAtLeast(f, b3, 2)
	check(err)
	fmt.Printf("%d bytes @ %d: %s\n", n3, o3, string(b3))

	p("=========================================================")
	p("There is no built-in rewind, but Seek(0, 0) accomplishes this.")
	p("=========================================================")
	_, err = f.Seek(0, 0)
	check(err)

	p("=========================================================")
	p("The bufio package implements a buffered reader that may be useful both for its efficiency with many small reads and because of the additional reading methods it provides.")
	p("=========================================================")
	r4 := bufio.NewReader(f)
	b4, err := r4.Peek(5)
	check(err)
	fmt.Printf("5 bytes: %s\n", string(b4))

	p("=========================================================")
	p("Close the file when you’re done (usually this would be scheduled immediately after Opening with defer).")
	p("=========================================================")
	f.Close()

	p("=========================================================")
	p("Passing the file path as a command line flag")
	p("=========================================================")
	fptr := flag.String("fpath", "test.txt", "file path to read from")
	flag.Parse()
	fmt.Println("value of fpath is", *fptr)
	data, err := ioutil.ReadFile(*fptr)
	if err != nil {
		fmt.Println("File reading error", err)
		return
	}
	fmt.Println("Contents of file:", string(data))
	p("********************************************************")
	p("WRITING FILES")
	p("********************************************************")
	p("=========================================================")
	p("To start, here’s how to dump a string (or just bytes) into a file.")
	p("=========================================================")
	d1 := []byte("hello\ngo\n")
	err = ioutil.WriteFile("/tmp/dat1", d1, 0644)
	check(err)
	p("=========================================================")
	p("For more granular writes, open a file for writing.")
	p("=========================================================")
	f, err = os.Create("test2.txt")
	check(err)

	p("=========================================================")
	p("It's idiomatic to defer a Close immediately after opening a file.")
	p("=========================================================")
	defer f.Close()

	p("=========================================================")
	p("You can Write byte slices as you’d expect.")
	p("=========================================================")
	d2 := []byte{115, 111, 109, 101, 10}
	n2, err = f.Write(d2)
	check(err)
	fmt.Printf("wrote %d bytes\n", n2)

	p("=========================================================")
	p("A WriteString is also available.")
	p("=========================================================")
	n3, err = f.WriteString("writes\n")
	fmt.Printf("wrote %d bytes\n", n3)

	p("=========================================================")
	p("Issue a Sync to flush writes to stable storage.")
	p("=========================================================")
	f.Sync()
	p("=========================================================")
	p("bufio provides buffered writers in addition to the buffered readers we saw earlier.")
	p("=========================================================")
	w := bufio.NewWriter(f)
	n4, err := w.WriteString("buffered\n")
	fmt.Printf("wrote %d bytes\n", n4)
	p("=========================================================")
	p("	Use Flush to ensure all buffered operations have been applied to the underlying writer.")
	p("=========================================================")
	w.Flush()

	p("=========================================================")
	p("Writing strings line by line to a file")
	p("=========================================================")
	f, err = os.Create("lines")
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	d := []string{"Welcome to the world of Go1.", "Go is a compiled language.", "It is easy to learn Go."}

	for _, v := range d {
		fmt.Fprintln(f, v)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("file written successfully")

	p("=========================================================")
	p("Appending to a file")
	p("=========================================================")
	f, err = os.OpenFile("lines", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	newLine := "File handling is easy."
	_, err = fmt.Fprintln(f, newLine)
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("file appended successfully")
}
