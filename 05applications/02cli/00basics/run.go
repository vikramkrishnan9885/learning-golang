package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {

	// COMMAND LINE ARGUMENTS

	// os.Args provides access to raw command-line arguments. Note that
	// the first value in this slice is the path to the program, and
	// os.Args[1:] holds the arguments to the program.

	argsWithProg := os.Args
	argsWithoutProg := os.Args[1:]

	// You can get individual args with normal indexing.

	arg := os.Args[3]
	fmt.Println(argsWithProg)
	fmt.Println(argsWithoutProg)
	fmt.Println(arg)

	// COMMAND LINE FLAGS

	// Basic flag declarations are available for string, integer, and
	// boolean options. Here we declare a string flag word with a default
	// value "foo" and a short description. This flag.String function
	// returns a string pointer (not a string value); we’ll see how to
	// use this pointer below.

	wordPtr := flag.String("word", "foo", "a string")

	// This declares numb and fork flags, using a similar approach to the
	// word flag.

	numbPtr := flag.Int("numb", 42, "an int")
	boolPtr := flag.Bool("fork", false, "a bool")

	// It's also possible to declare an option that uses an existing var
	// declared elsewhere in the program. Note that we need to pass in a
	// pointer to the flag declaration function.

	var svar string
	flag.StringVar(&svar, "svar", "bar", "a string var")

	// Once all flags are declared, call flag.Parse() to execute the
	// command-line parsing.

	flag.Parse()

	// Here we’ll just dump out the parsed options and any trailing
	// positional arguments. Note that we need to dereference the pointers
	// with e.g. *wordPtr to get the actual option values.

	fmt.Println("word:", *wordPtr)
	fmt.Println("numb:", *numbPtr)
	fmt.Println("fork:", *boolPtr)
	fmt.Println("svar:", svar)
	fmt.Println("tail:", flag.Args())

	// SUB-COMMANDS
	// Some command-line tools, like the go tool or git have many subcommands,
	// each with its own set of flags. For example, go build and go get are
	// two different subcommands of the go tool. The flag package lets us
	// easily define simple subcommands that have their own flags.

	// We declare a subcommand using the NewFlagSet function, and proceed
	// to define new flags specific for this subcommand.

	fooCmd := flag.NewFlagSet("foo", flag.ExitOnError)
	fooEnable := fooCmd.Bool("enable", false, "enable")
	fooName := fooCmd.String("name", "", "name")

	// For a different subcommand we can define different supported flags.

	barCmd := flag.NewFlagSet("bar", flag.ExitOnError)
	barLevel := barCmd.Int("level", 0, "level")

	// The subcommand is expected as the first argument to the program.

	if len(os.Args) < 2 {
		fmt.Println("expected 'foo' or 'bar' subcommands")
		os.Exit(1)
	}

	// Check which subcommand is invoked.

	switch os.Args[1] {

	//	For every subcommand, we parse its own flags and have access to
	// trailing positional arguments.

	case "foo":
		fooCmd.Parse(os.Args[2:])
		fmt.Println("subcommand 'foo'")
		fmt.Println("  enable:", *fooEnable)
		fmt.Println("  name:", *fooName)
		fmt.Println("  tail:", fooCmd.Args())
	case "bar":
		barCmd.Parse(os.Args[2:])
		fmt.Println("subcommand 'bar'")
		fmt.Println("  level:", *barLevel)
		fmt.Println("  tail:", barCmd.Args())
	default:
		fmt.Println("expected 'foo' or 'bar' subcommands")
		os.Exit(1)
	}

	// ENVIRONMENT VARIABLES

	// To set a key/value pair, use os.Setenv. To get a value for a key,
	// use os.Getenv. This will return an empty string if the key isn’t
	// present in the environment.

	os.Setenv("FOO", "1")
	fmt.Println("FOO:", os.Getenv("FOO"))
	fmt.Println("BAR:", os.Getenv("BAR"))

	// Use os.Environ to list all key/value pairs in the environment. This
	// returns a slice of strings in the form KEY=value. You can strings.
	// Split them to get the key and value. Here we print all the keys.

	fmt.Println()
	for _, e := range os.Environ() {
		pair := strings.Split(e, "=")
		fmt.Println(pair[0])
	}

}
