package main

import "fmt"

func zeroval(ival int) {
	ival = 0
	//  zeroval has an int parameter, so arguments will be passed to it
	// by value. zeroval will get a copy of ival distinct from the one in
	// the calling function.
}

func zeroptr(iptr *int) {
	*iptr = 0
	// zeroptr in contrast has an *int parameter, meaning that it takes an
	// int pointer. The *iptr code in the function body then dereferences
	// the pointer from its memory address to the current value at that
	// address. Assigning a value to a dereferenced pointer changes the
	// value at the referenced address.
}

// Returning a pointer from function
func hello() *int {
	i := 5
	return &i
}

func main() {

	i := 1
	fmt.Println("initial:", i)
	zeroval(i)
	fmt.Println("zeroval:", i)

	zeroptr(&i) //The &i syntax gives the memory address of i, i.e. a pointer to i.
	fmt.Println("zeroptr:", i)

	fmt.Println("pointer:", &i) // Pointers can be printed too.

	// The zero value of a pointer is nil.
	a := 25
	var b *int
	if b == nil {
		fmt.Println("b is", b)
		b = &a
		fmt.Println("b after initialization is", b)
	}

	// Creating pointers using the new function
	size := new(int)
	fmt.Printf("Size value is %d, type is %T, address is %v\n", *size, size, size)
	*size = 85
	fmt.Println("New size value is", *size)

	// Dereferencing a pointer - use *
	x := 255
	y := &x
	fmt.Println("address of x is", y)
	fmt.Println("value of x is", *y)

	// we change the value in b using the pointer.
	p := 255
	q := &p
	fmt.Println("address of p is", q)
	fmt.Println("value of p is", *q)
	*q++
	fmt.Println("new value of p is", p)

	d := hello()
	fmt.Println("Value of d", *d)
	// Go does not support pointer arithmetic which is present in other
	// languages like C and C++.

}
