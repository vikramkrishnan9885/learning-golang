package main

import "fmt"

func main() {

	fmt.Println("==========================================================")
	fmt.Println("IF WITH ELSE")
	fmt.Println("==========================================================")
	n := 7
	if n%2 == 0 {
		fmt.Println("n is even")
	} else { // else cannot be on the next line
		fmt.Println("n is odd")
	}

	fmt.Println("==========================================================")
	fmt.Println("IF WITHOUT ELSE")
	fmt.Println("==========================================================")
	n = 8
	if n%2 == 0 {
		fmt.Println("n is even")
	}

	fmt.Println("==========================================================")
	fmt.Println("IF WITH MULTIPLE ELSE")
	fmt.Println("==========================================================")
	if n < 0 {
		fmt.Println("n is negative")
	} else if n < 10 {
		fmt.Println(" note it is else if, not else or not elif")
		fmt.Println(" n less than 10")
	} else {
		fmt.Println("else block")
	}

	// Golang does not have a ternary operator

	fmt.Println("==========================================================")
	fmt.Println("STATEMENT WITH CONDITIONAL")
	fmt.Println("==========================================================")
	if num := 10; num%2 == 0 { //checks if number is even
		fmt.Println(num, "is even")
	} else {
		fmt.Println(num, "is odd")
	}
}
