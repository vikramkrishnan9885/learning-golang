package main

import (
	"fmt"
	"time"
)

func main() {

	fmt.Println("==========================================================")
	fmt.Println("BASIC SWITCH CASE")
	fmt.Println("==========================================================")
	i := 2
	fmt.Print("Write ", i, " as ")
	switch i {
	case 1:
		fmt.Println("one")
	case 2:
		fmt.Println("two")
	default: // default case
		fmt.Println("default")
	}

	fmt.Println("==========================================================")
	fmt.Println("MULTIPLE EXPRESSIONS")
	fmt.Println("==========================================================")
	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		fmt.Println("Weekend")
	default:
		fmt.Println("Weekday")
	}

	fmt.Println("==========================================================")
	fmt.Println("EXPRESSIONLESS SWITCH IS EQUIVALENT TO IF/ELSE")
	fmt.Println("==========================================================")
	num := 75
	switch { // expression is omitted
	case num >= 0 && num <= 50:
		fmt.Println("num is greater than 0 and less than 50")
	case num >= 51 && num <= 100:
		fmt.Println("num is greater than 51 and less than 100")
	case num >= 101:
		fmt.Println("num is greater than 100")
	}

	fmt.Println("==========================================================")
	fmt.Println("TYPE SWITCHES")
	fmt.Println("==========================================================")
	whatAmI := func(i interface{}) {
		switch i.(type) {
		case bool:
			fmt.Println("I'm a bool")
		case int:
			fmt.Println("Integer")
		default:
			fmt.Println("No idea, dude")
		}
	}
	whatAmI(true)
	whatAmI(1)
	whatAmI("hey")
}
