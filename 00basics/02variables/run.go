package main

import "fmt"

func main() {

	// Variables are automatically initialized to the zero value of the
	//variables type
	fmt.Println("==========================================================")
	fmt.Println("DECLARATION AND ASSIGNMENT")
	fmt.Println("==========================================================")
	var a int // Declare a variable
	fmt.Println(a)
	a = 24 // assignment
	fmt.Println(a)
	a = 59 // variables are mutable
	fmt.Println(a)

	fmt.Println("==========================================================")
	fmt.Println("MULTIPLE ASSIGNMENTS")
	fmt.Println("==========================================================")
	var b, c int = 1, 2 // multiple assigments
	fmt.Println(b, c)

	fmt.Println("==========================================================")
	fmt.Println("TYPE INFERENCE")
	fmt.Println("==========================================================")
	var d = true // type inference
	fmt.Println(d)

	fmt.Println("==========================================================")
	fmt.Println(":= SYNTAX")
	fmt.Println("==========================================================")
	f := "apply" // The := syntax is shorthand for declaring and initializing a variable
	fmt.Println(f)

	x, y := 20, 30 // declare variables x and y
	fmt.Println("x is", x, "y is", y)
	y, z := 40, 50 // y is already declared but z is new
	fmt.Println("y is", y, "z is", z)
	y, z = 80, 90 // assign new values to already declared variables b and c
	fmt.Println("changed y is", y, "z is", z)
	// x, y := 40, 50 //error, no new variables

	fmt.Println("==========================================================")
	fmt.Println("VARIABLE BLOCKS")
	fmt.Println("==========================================================")
	var (
		age  = 30
		name = "alice"
	) // variable blocks
	fmt.Println("age is, ", age, "name is, ", name)

	fmt.Println("==========================================================")
	fmt.Println("COMPLEX NUMBERS")
	fmt.Println("==========================================================")
	c1 := complex(5, 7) // Complex numbers
	c2 := 3 + 2i
	csum := c1 + c2
	fmt.Println(csum)

	fmt.Println("==========================================================")
	fmt.Println("TYPE INFERENCE")
	fmt.Println("==========================================================")
	i := 55   // int
	j := 67.8 // float64
	// sum0 := i + j // Unlike C, this will not work
	sum1 := i + int(j)
	sum2 := float64(i) + j
	fmt.Println(sum1)
	fmt.Println(sum2)
}
