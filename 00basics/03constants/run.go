package main

import "fmt"

func main() {

	const s string = "Hello, constant"
	fmt.Println(s)
	// s = "try reassigning" // WON'T WORK

	const n = 1000
	fmt.Println(n)

	const d = 3e20 / n
	fmt.Println(int64(d))
}
