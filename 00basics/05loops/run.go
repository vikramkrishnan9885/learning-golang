package main

import "fmt"

func main() {
	fmt.Println("==========================================================")
	fmt.Println("The most basic type, with a single condition.")
	fmt.Println("==========================================================")
	i := 1
	for i <= 3 {
		fmt.Println(i)
		i = i + 1
	}

	fmt.Println("==========================================================")
	fmt.Println("A classic initial/condition/after for loop.")
	fmt.Println("==========================================================")
	for j := 7; j <= 9; j++ {
		fmt.Println(j)
	}
	fmt.Println("==========================================================")
	fmt.Println("for without a condition will loop repeatedly until you break out of the loop or return from the enclosing function.")
	fmt.Println("==========================================================")
	for {
		fmt.Println("loop")
		break
	}

	fmt.Println("==========================================================")
	fmt.Println("You can also continue to the next iteration of the loop.")
	fmt.Println("==========================================================")
	for n := 0; n <= 5; n++ {
		if n%2 == 0 {
			continue
		}
		fmt.Println(n)
	}

	fmt.Println("==========================================================")
	fmt.Println("Break.")
	fmt.Println("==========================================================")
	for i := 1; i <= 10; i++ {
		if i > 5 {
			break //loop is terminated if i > 5
		}
		fmt.Printf("%d ", i)
	}
	fmt.Printf("\nline after for loop\n")

	fmt.Println("==========================================================")
	fmt.Println("Continue: skip the current iteration of the for loop. All code present in a for loop after the continue statement will not be executed for the current iteration. The loop will move on to the next iteration.")
	fmt.Println("==========================================================")
	for i := 1; i <= 10; i++ {
		if i%2 == 0 {
			continue
		}
		fmt.Printf("%d ", i)
		fmt.Println()
	}

	fmt.Println("==========================================================")
	fmt.Println("Nested loops.")
	fmt.Println("==========================================================")
	n := 5
	for i := 0; i < n; i++ {
		for j := 0; j <= i; j++ {
			fmt.Print("*")
		}
		fmt.Println()
	}

	fmt.Println("==========================================================")
	fmt.Println("Nested loops with labels and breaks")
	fmt.Println("==========================================================")
outer:
	for i := 0; i < 3; i++ {
		for j := 1; j < 4; j++ {
			fmt.Printf("i = %d , j = %d\n", i, j)
			if i == j {
				break outer
			}
		}

	}
}
