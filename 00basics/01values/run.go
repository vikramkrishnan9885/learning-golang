package main

import "fmt"

func main() {

	fmt.Println("==========================================================")
	fmt.Println("STRINGS")
	fmt.Println("==========================================================")
	fmt.Println("go" + "lang")

	fmt.Println("==========================================================")
	fmt.Println("NUMBERS")
	fmt.Println("==========================================================")
	fmt.Println("1+1 = ", 1+1)
	fmt.Println("7.0/3.0")

	fmt.Println("==========================================================")
	fmt.Println("BOOLEAN")
	fmt.Println("==========================================================")
	fmt.Println(true && false)
	fmt.Println(true || false)
	fmt.Println(true)
}
