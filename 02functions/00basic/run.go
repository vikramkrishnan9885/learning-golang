package main

import "fmt"

func plus(a int, b int) int {
	// Go requires explicit returns, i.e. it won’t automatically return
	// the value of the last expression.
	return a + b
}

// When you have multiple consecutive parameters of the same type,
// you may omit the type name for the like-typed parameters up to the
// final parameter that declares the type.
func plusPlus(a, b, c int) int {
	return a + b + c
}

func vals() (int, int) {
	return 3, 7
}

// Named return values
// It is possible to return named values from a function. If a return value
// is named, it can be considered as being declared as a variable in the
// first line of the function
func rectProps(length, width float64) (area, perimeter float64) {
	area = length * width
	perimeter = (length + width) * 2
	return //no explicit return value
}

// Recursion
func fact(n int) int {
	if n == 0 {
		return 1
	}
	return n * fact(n-1)
}

func main() {

	// Call a function just as you’d expect, with name(args).
	res := plus(1, 2)
	fmt.Println("1+2 =", res)
	res = plusPlus(1, 2, 3)
	fmt.Println("1+2+3 =", res)

	a, b := vals()
	fmt.Println(a)
	fmt.Println(b)

	// If you only want a subset of the returned values,
	// use the blank identifier _.
	_, c := vals()
	fmt.Println(c)

	area, _ := rectProps(10.8, 5.6) // perimeter is discarded
	fmt.Printf("Area %f \n ", area)

	fmt.Println(fact(7))
}
