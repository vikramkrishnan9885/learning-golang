package main

import (
	"fmt"
)

type Employee struct {
	name     string
	salary   int
	currency string
	age      int
}

/*
 displaySalary() method has Employee as the receiver type
*/
func (e Employee) displaySalary() {
	fmt.Printf("Salary of %s is %s%d", e.name, e.currency, e.salary)
}

/*
 displaySalary() method converted to function with Employee as parameter

 Methods with the same name can be defined on different types whereas
 functions with the same names are not allowed.

 Methods allow a logical grouping of behavior related to a type similar
 to classes.
*/
func displaySalary(e Employee) {
	fmt.Printf("Salary of %s is %s%d", e.name, e.currency, e.salary)
}

/*
Method with value receiver
*/
func (e Employee) changeName(newName string) {
	e.name = newName
}

/*
Method with pointer receiver
The difference between value and pointer receiver is, changes made inside
a method with a pointer receiver is visible to the caller whereas this is
not the case in value receiver.

When to use pointer receiver and when to use value receiver
==============================================================
Generally, pointer receivers can be used when changes made to the receiver
inside the method should be visible to the caller.

Pointers receivers can also be used in places where it's expensive to copy
a data structure. Consider a struct that has many fields. Using this struct
as a value receiver in a method will need the entire struct to be copied
which will be expensive. In this case, if a pointer receiver is used, the
struct will not be copied and only a pointer to it will be used in the method.

In all other situations, value receivers can be used.
*/
func (e *Employee) changeAge(newAge int) {
	e.age = newAge
}

/*
Value receivers in methods vs Value arguments in functions
============================================================

Similar point applies to pointer receivers

When a function has a value argument, it will accept only a value argument.

When a method has a value receiver, it will accept both pointer and value receivers.
*/

type rectangle struct {
	length int
	width  int
}

func area(r rectangle) {
	fmt.Printf("\nArea Function result: %d\n", (r.length * r.width))
}

func (r rectangle) area() {
	fmt.Printf("\nArea Method result: %d\n", (r.length * r.width))
}

/*
Pointer receivers in methods vs Pointer arguments in functions
==================================================================
Similar to value arguments, functions with pointer arguments will accept
only pointers whereas methods with pointer receivers will accept both pointer
and value receiver.
*/

func perimeter(r *rectangle) {
	fmt.Println("\nperimeter function output:", 2*(r.length+r.width))

}

func (r *rectangle) perimeter() {
	fmt.Println("\nperimeter method output:", 2*(r.length+r.width))
}

/*
Methods with non-struct receivers
==============================================

So far we have defined methods only on struct types. It is also possible
to define methods on non-struct types, but there is a catch. To define a
method on a type, the definition of the receiver type and the definition
of the method should be present in the same package.

program will throw compilation error cannot define new methods on
non-local type int

The way to get this working is to create a type alias for the built-in type
int and then create a method with this type alias as the receiver.
*/

type myInt int

func (a myInt) add(b myInt) myInt {
	return a + b
}

func main() {
	emp1 := Employee{
		name:     "Sam Adolf",
		salary:   5000,
		currency: "$",
	}
	emp1.displaySalary() //Calling displaySalary() method of Employee type
	displaySalary(emp1)

	e := Employee{
		name: "Mark Andrew",
		age:  50,
	}
	fmt.Printf("\nEmployee name before change: %s", e.name)
	e.changeName("Michael Andrew")
	fmt.Printf("\nEmployee name after change: %s", e.name)

	fmt.Printf("\n\nEmployee age before change: %d", e.age)
	e.changeAge(51)
	fmt.Printf("\nEmployee age after change: %d", e.age)

	r := rectangle{
		length: 10,
		width:  5,
	}
	area(r)
	r.area()

	p := &r
	/*
	   compilation error, cannot use p (type *rectangle) as type rectangle
	   in argument to area
	*/
	//area(p)

	p.area() //calling value receiver with a pointer

	perimeter(p)
	p.perimeter()
	/*
	   cannot use r (type rectangle) as type *rectangle in argument to perimeter
	*/
	//perimeter(r)
	r.perimeter() //calling pointer receiver with a value

	num1 := myInt(5)
	num2 := myInt(10)
	sum := num1.add(num2)
	fmt.Println("Sum is", sum)
}
