package main

import (
	"fmt"
	"net/http"
)

func main() {
	// PROCESS DYNAMIC REQUESTS
	// Registering a request handler to the default HTTP Server using HandleFunc
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		/*
			First, create a Handler which receives all incomming HTTP
			connections from browsers, HTTP clients or API requests.
			A handler in Go is a function with this signature:

				func (w http.ResponseWriter, r *http.Request)

			The function receives two parameters:
				An http.ResponseWriter which is where you write your text/html response to.
				An http.Request which contains all information about this HTTP
				request including things like the URL or header fields.
		*/
		fmt.Fprintf(w, "Hello you have requested %s\n", r.URL.Path)
	})

	// SERVING STATIC ASSETS
	// To serve static assets like JavaScript, CSS and images, we use the
	// inbuilt http.FileServer and point it to a url path. For the file
	// server to work properly it needs to know, where to serve files from.
	// We can do this like so:

	fs := http.FileServer(http.Dir("static/"))

	// Once our file server is in place, we just need to point a url path at
	// it, just like we did with the dynamic requests. One thing to note:
	// In order to serve files correctly, we need to strip away a part of
	// the url path. Usually this is the name of the directory our files live in.

	http.Handle("/static/", http.StripPrefix("/static/", fs))

	// ACCEPT CONNECTIONS
	http.ListenAndServe(":8081", nil)
	// The last thing to finish off our basic HTTP server is, to listen on a
	// port to accept connections from the internet. As you can guess, Go has also
	// an inbuilt HTTP server, we can start faily quickly. Once started, you
	//  can view your HTTP server in your browser
	// The request handler alone can not accept any HTTP connections from
	// the outside. An HTTP server has to listen on a port to pass connections
	// on to the request handler.

}
