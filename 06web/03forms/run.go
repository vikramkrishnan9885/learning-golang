// forms.go
package main

import (
	"html/template"
	"net/http"
)

type ContactDetails struct {
	Email   string
	Subject string
	Message string
}

func main() {
	/*
		The three most important and most frequently used functions are:
			New — allocates new, undefined template,
			Parse — parses given template string and return parsed template,
			Execute — applies parsed template to the data structure and writes result to the given writer.
	*/
	tmpl := template.Must(template.ParseFiles("forms.html"))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// If Method is not Post, pass nil as data
		if r.Method != http.MethodPost {
			tmpl.Execute(w, nil)
			return
		}

		// Extract values from the http request
		details := ContactDetails{
			Email:   r.FormValue("email"),
			Subject: r.FormValue("subject"),
			Message: r.FormValue("message"),
		}

		// do something with details
		_ = details

		// Set success to true, so that we can display the message
		tmpl.Execute(w, struct{ Success bool }{true})
	})

	http.ListenAndServe(":8080", nil)
}
