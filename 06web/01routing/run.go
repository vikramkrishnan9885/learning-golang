package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	// Go’s net/http package provides a lot of functionalities for the HTTP
	// protocol. One thing it doesn’t do very well is complex request routing
	// like segmenting a request url into single parameters.
	// gorilla/mux is a package which adapts to Go’s default HTTP router.
	//It is also compliant to Go’s default request handler signature
	// func (w http.ResponseWriter, r *http.Request), so the package can be
	// mixed and machted with other HTTP libraries like middleware or
	// exisiting applications.
)

func main() {
	// CREATING NEW ROUTER
	// First create a new request router. The router is the main router for your web application and will later be passed as parameter to the server. It will receive all HTTP connections and pass it on to the request handlers you will register on it
	r := mux.NewRouter()

	// REGISTERING A NEW HANDLER
	// Once you have a new router you can register request handlers like usual.
	// The only difference is, that instead of calling http.HandleFunc(...),
	// you call HandleFunc on your router like this: r.HandleFunc(...).
	r.HandleFunc("/books/{title}/page/{page}", func(w http.ResponseWriter, r *http.Request) {
		// The last thing is to get the data from these segments. The package
		// comes with the function mux.Vars(r) which takes the http.Request as
		// parameter and returns a map of the segments.
		vars := mux.Vars(r)
		title := vars["title"]
		page := vars["page"]

		fmt.Fprintf(w, "You've requested the book: %s on page %s\n", title, page)
	})

	http.ListenAndServe(":8081", r)
	/*
		Features of the gorilla/mux Router
			Methods
			==========
			Restrict the request handler to specific HTTP methods.

			r.HandleFunc("/books/{title}", CreateBook).Methods("POST")
			r.HandleFunc("/books/{title}", ReadBook).Methods("GET")
			r.HandleFunc("/books/{title}", UpdateBook).Methods("PUT")
			r.HandleFunc("/books/{title}", DeleteBook).Methods("DELETE")

			Hostnames & Subdomains
			=========================
			Restrict the request handler to specific hostnames or subdomains.

			r.HandleFunc("/books/{title}", BookHandler).Host("www.mybookstore.com")

			Schemes
			===========
			Restrict the request handler to http/https.

			r.HandleFunc("/secure", SecureHandler).Schemes("https")
			r.HandleFunc("/insecure", InsecureHandler).Schemes("http")

			Path Prefixes & Subrouters
			=================================
			Restrict the request handler to specific path prefixes.

			bookrouter := r.PathPrefix("/books").Subrouter()
			bookrouter.HandleFunc("/", AllBooks)
			bookrouter.HandleFunc("/{title}", GetBook)
	*/
}
