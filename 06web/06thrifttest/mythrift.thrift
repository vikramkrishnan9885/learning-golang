/*
 * 
 * Namespaces
 * Namespaces in Thrift are akin to namespaces in C++ or packages in Java — they offer a convenient way of organizing (or isolating) your code. Namespaces may also be used to prevent name clashes between type definitions.
 * Because each language has its own package-like mechanisms (e.g. Python has modules), Thrift allows you to customize the namespace behavior on a per-language basis:
 * namespace cpp com.example.project  // 1
 * namespace java com.example.project // 2
 * (1) Translates to namespace com { namespace example { namespace project {
 * (2) Translates to package com.example.project
*/

namespace go mythrift.demo

/*
 * https://thrift.apache.org/docs/types
 *
*/

struct Article{
    1: i32 id,
    2: string title,
    3: string content,
    4: string author,
}

const map<string,string> MAPCONSTANT = {'hello':'world', 'goodnight':'moon'}

service myThrift {
        list<string> CallBack(1:i64 callTime, 2:string name, 3:map<string, string> paramMap),
        void put(1: Article newArticle),
}