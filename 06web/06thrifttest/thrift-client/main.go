package main

import (
	"context"
	"fmt"
	"learning/06web/06thrifttest/gen-go/mythrift/demo"
	"net"
	"os"
	"strconv"
	"time"

	"git.apache.org/thrift.git/lib/go/thrift"
)

const (
	HOST = "127.0.0.1"
	PORT = "9090"
)

func first(args ...interface{}) interface{} {
	return args[0]
}

// THE ABOVE CODE JUST DIDN'T WORK

func main() {
	startTime := currentTimeMillis()

	defaultCtx := context.Background()
	// Read about Context package. This seems to be golang specific

	// The below is the classic: Transport, Protocol, Socket, Client bit
	transportFactory := thrift.NewTFramedTransportFactory(thrift.NewTTransportFactory())
	protocolFactory := thrift.NewTBinaryProtocolFactoryDefault()

	x := net.JoinHostPort(HOST, PORT)
	// Had to add the above line.

	transport, err := thrift.NewTSocket(x)
	if err != nil {
		fmt.Fprintln(os.Stderr, "error resolving address:", err)
		os.Exit(1)
	}

	useTransport, _ := transportFactory.GetTransport(transport)
	// Had to edit the above line.
	client := demo.NewMyThriftClientFactory(useTransport, protocolFactory)
	// Note what inputs we are giving to the client factory
	if err := transport.Open(); err != nil {
		fmt.Fprintln(os.Stderr, "Error opening socket to "+HOST+":"+PORT, " ", err)
		os.Exit(1)
	}
	defer transport.Close()

	for i := 0; i < 10; i++ {
		paramMap := make(map[string]string)
		paramMap["a"] = "mythrift.demo"
		paramMap["b"] = "test" + strconv.Itoa(i+1)
		r1, _ := client.CallBack(defaultCtx, time.Now().Unix(), "Go client", paramMap)
		// Had to edit the above line.
		fmt.Println("Go client call->", r1)
	}

	model := demo.Article{1, "Send from Go Thrift Client", "Hello World!", "Go"}
	client.Put(defaultCtx, &model)
	// Had to edit the above line.
	endTime := currentTimeMillis()
	fmt.Printf("The call took:%d-%d=%d Millis \n", endTime, startTime, (endTime - startTime))

}

func currentTimeMillis() int64 {
	return time.Now().UnixNano() / 1000000
}
