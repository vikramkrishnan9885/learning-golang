package main

import (
	"errors"
	"fmt"
	"os"
)

// In Go it’s idiomatic to communicate errors via an explicit,
// separate return value. This contrasts with the exceptions used in
// languages like Java and Ruby and the overloaded single result / error
// value sometimes used in C. Go’s approach makes it easy to see which
// functions return errors and to handle them using the same language
// constructs employed for any other, non-error tasks.

func f1(arg int) (int, error) { // By convention, errors are the last return value and have type error, a built-in interface
	if arg == 42 {
		return -1, errors.New("can't work with 42") // errors.New constructs a basic error value with the given error message.
	} else {
		return arg + 3, nil // A nil value in the error position indicates that there was no error.
	}
}

func main() {
	f, err := os.Open("/test.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(f.Name(), "opened successfully")
}
