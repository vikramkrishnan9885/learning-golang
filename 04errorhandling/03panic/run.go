package main

import (
	"fmt"
	"os"
)

/**
When should panic be used?
=================================
One important factor is that you should avoid panic and recover and use
errors where ever possible. Only in cases where the program just cannot
continue execution should a panic and recover mechanism be used.

There are two valid use cases for panic.

An unrecoverable error where the program cannot simply continue its execution.
One example would be a web server which fails to bind to the required port.
In this case it's reasonable to panic as there is nothing else to do if the
port binding itself fails.

A programmer error.
Let's say we have a method which accepts a pointer as a parameter and
someone calls this method using nil as argument. In this case we can panic
as it's a programmer error to call a method with nil argument which was
expecting a valid pointer.
*/

func main() {

	// Recover
	// recover is a builtin function which is used to regain control of
	// a panicking goroutine.
	defer fmt.Println("deferred call in main")
	firstName := "Elon"
	fullName(&firstName, nil)
	fmt.Println("returned normally from main")

	if os.Args[1] == "1" {
		// We'll use panic throughout this site to check for unexpected errors.
		// This is the only program on the site designed to panic.
		panic("a problem")
	}
	// A common use of panic is to abort if a function returns an error
	// value that we don’t know how to (or want to) handle. Here’s an
	// example of panicking if we get an unexpected error when creating a
	// new file.
	_, err := os.Create("/tmp/file")
	if err != nil {
		panic(err)
	}

}

func recoverName() {
	if r := recover(); r != nil {
		fmt.Println("recovered from ", r)
	}
}

func fullName(firstName *string, lastName *string) {
	defer recoverName()
	if firstName == nil {
		panic("runtime error: first name cannot be nil")
	}
	if lastName == nil {
		panic("runtime error: last name cannot be nil")
	}
	fmt.Printf("%s %s\n", *firstName, *lastName)
	fmt.Println("returned normally from fullName")
}
