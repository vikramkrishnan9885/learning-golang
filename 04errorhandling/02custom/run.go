package main

import (
	"fmt"
	"net"
	"os"
	"path/filepath"
)

type PathError struct {
	Op   string
	Path string
	Err  error
}

func (e *PathError) Error() string { return e.Op + " " + e.Path + ": " + e.Err.Error() }

func main() {
	fmt.Println("===========================================================")
	fmt.Println("Asserting the underlying struct type and getting more information from the struct fields")
	fmt.Println("===========================================================")
	f, err := os.Open("/test.txt")
	if err, ok := err.(*os.PathError); ok {
		fmt.Println("Please note that the file at path", err.Path, "failed to open")
		return
	}
	fmt.Println(f.Name(), "opened successfully")

	fmt.Println("===========================================================")
	fmt.Println("Asserting the underlying struct type and getting more information using methods")
	fmt.Println("===========================================================")
	addr, err := net.LookupHost("golangbot123.com")
	if err, ok := err.(*net.DNSError); ok {
		if err.Timeout() {
			fmt.Println("operation timed out")
		} else if err.Temporary() {
			fmt.Println("temporary error")
		} else {
			fmt.Println("generic error: ", err)
		}
		return
	}
	fmt.Println(addr)

	fmt.Println("===========================================================")
	fmt.Println("Direct Comparison")
	fmt.Println("===========================================================")
	files, error := filepath.Glob("[")
	if error != nil && error == filepath.ErrBadPattern {
		fmt.Println(error)
		return
	}
	fmt.Println("matched files", files)

}
